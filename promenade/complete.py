import re
from typing import Optional

from cache import AsyncTTL  # type: ignore
import httpx
from prompt_toolkit.completion import Completer
from prompt_toolkit.completion.word_completer import WordCompleter


class PromCompleter(Completer):

    "Completes the most common PromQL constructs by querying the server"

    METRIC_NAME = re.compile("[a-zA-Z0-9_:]*")

    def __init__(self, client: httpx.AsyncClient):
        self.client = client

    def get_completions(self, document, complete_event):
        pass

    def _find_metric(self, rtext: str):
        m = re.match(self.METRIC_NAME, rtext.strip())
        if m:
            return m.group(0)[::-1]

    @AsyncTTL(time_to_live=60)
    async def _query(self, url: str, match: Optional[str] = None):
        "Small wrapper for HTTP client"
        if match:
            result = await self.client.get(url, params={"match[]": [match]})
        else:
            result = await self.client.get(url)
        return result.json()["data"]

    async def get_completions_async(self, document, complete_event):
        # Some messy half-assed parsing to get context sensitive completion.
        # Probably buggy.
        text = document.text_before_cursor.lstrip()
        rtext = text[::-1]  # reverse text to make it easier to traverse
        m = re.search("[^a-zA-Z0-9_:].*", rtext)
        if m:
            rrest = m.group(0).strip()
            c0 = rrest[0]
            if c0 in "{,":
                # OK, looks like a label name
                beginning = rrest.index("{")
                metric = self._find_metric(rrest[beginning + 1:].strip())
                try:
                    series = await self._query("/series", metric)
                    if not series:
                        return
                except KeyError:
                    return

                labels_set = set.union(*(set(s.keys()) for s in series))
                all_labels = (f"{lab}="
                              for lab in labels_set
                              if lab != "__name__")
                completer = WordCompleter(all_labels,
                                          ignore_case=True,
                                          pattern=self.METRIC_NAME)
                for c in completer.get_completions(document, complete_event):
                    yield c

            elif c0 in '="':
                # Should be a label value
                beginning = rrest.index("{")
                metric = self._find_metric(rrest[beginning + 1:])

                # Looking for a label name
                m = re.search("[a-zA-Z0-9_:]+", rrest)
                if m:
                    label = m.group(0)[::-1]
                else:
                    return

                try:
                    all_values = await self._query(f"/label/{label}/values", metric)
                except KeyError:
                    return
                values = (f'"{v}"' for v in all_values if v != "__name__")
                completer = WordCompleter(values,
                                          ignore_case=True,
                                          match_middle=True,
                                          pattern=re.compile('[^=]*'))
                for c in completer.get_completions(document, complete_event):
                    yield c

            elif c0 in "(":
                # Seems like a metric name
                metrics = await self._query("/label/__name__/values")
                completer = WordCompleter(metrics,
                                          ignore_case=True,
                                          match_middle=True,
                                          pattern=self.METRIC_NAME)
                for c in completer.get_completions(document, complete_event):
                    yield c
        else:
            # OK assuming it is a metric name
            metrics = await self._query("/label/__name__/values")
            completer = WordCompleter(metrics,
                                      ignore_case=True,
                                      match_middle=True,
                                      pattern=self.METRIC_NAME)
            for c in completer.get_completions(document, complete_event):
                yield c
