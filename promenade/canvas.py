"""
A space on the screen where we can draw freely, using Braille characters
for pixels. Supports line drawing and text, with color.
"""

from collections import Counter
from typing import Tuple, Dict


BRAILLE_ZERO = 10240

ANSI_COLORS = (
    [f'\033[1;3{x}m' for x in range(1, 8)] +
    [f'\033[0;3{x}m' for x in range(1, 8)]
)
ANSI_RESET = "\033[0;39m"


class BrailleCanvas:

    def __init__(self, width: int = 80, height: int = 20):
        self.width = width
        self.height = height
        self.w = width * 2  # width in braille dots
        self.h = height * 4  # height in braille dots
        self.pixels: Dict[Tuple[int, int], Dict[int, int]] = {}
        self.letters: Dict[Tuple[int, int], Tuple[str, int]] = {}

    def draw_pixel(self, x: int, y: int, color: int):
        x1, y1 = x // 2, y // 4
        if any([x1 < 0, x1 >= self.width, y1 < 0, y1 >= self.height]):
            return

        # The Braille Unicode block is ordered a bit strangely, where the
        # first fourth only deals with six dot patterns. So we treat bottom
        # row pixels separately

        x2, y2 = x % 2, y % 4
        if y2 < 3:
            p = 2 ** (y2 + 3 * x2)
        elif x2 == 0:
            p = 64
        elif x2 == 1:
            p = 128
        self.pixels.setdefault((x1, y1), {})[p] = color % 15

    def draw_line(self, p0: Tuple[int, int], p1: Tuple[int, int],
                  color: int = -1):

        """ Draw a straight line from p0 to p1 using a color. """

        x, y = p0
        x0, y0 = p0
        x1, y1 = p1
        dx = abs(x1 - x)
        sx = 1 if x < x1 else -1
        dy = -abs(y1 - y)
        sy = 1 if y < y1 else -1
        err = dx + dy

        self.draw_pixel(x, y, color)
        while x != x1 or y != y1:
            e2 = 2 * err
            if e2 >= dy:
                err += dy
                x += sx
            if e2 <= dx:
                err += dx
                y += sy
            self.draw_pixel(x, y, color)

    def draw_text(self, x: int, y: int, text: str, color: int = -1):
        for i, char in enumerate(text):
            self.letters[x + i, y] = (char, color)

    def _build_braille(self, subpixels: Dict[int, int],
                       ordered: bool = True):
        if ordered:
            # The last written pixel determines color
            color = list(subpixels.values())[-1]
        else:
            # Majority vote determines color
            colors = Counter(subpixels.values())
            color = colors.most_common()[0][0]
        return chr(BRAILLE_ZERO + sum(subpixels)), color

    def render(self):

        """ Build a list of printable strings out of the current canvas. """

        buf = []
        current_color = None
        for y in range(self.height):
            line = ""
            for x in range(self.width):
                pos = (x, y)
                letter = self.letters.get(pos)
                if letter is not None:
                    char, color = letter
                    if color != current_color:
                        line += ANSI_COLORS[color] if color is not None else ANSI_RESET
                        current_color = color
                    line += char
                else:
                    pixels = self.pixels.get(pos)
                    if pixels is None:
                        line += " "
                        continue
                    char, color = self._build_braille(pixels)
                    if color != current_color:
                        line += ANSI_COLORS[color]
                        current_color = color
                    line += char
            line += ANSI_RESET
            buf.append(line)
            current_color = None
        return buf


if __name__ == "__main__":
    canvas = BrailleCanvas(height=20, width=80)
    canvas.draw_line((0, 0), (80, 20), 3)
    canvas.draw_line((80, 0), (0, 20), 1)
    print(canvas.render())
