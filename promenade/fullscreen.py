import asyncio
from datetime import datetime
from dataclasses import dataclass
import os
import shutil
from time import time
from typing import Tuple, Optional

from appdirs import user_data_dir  # type: ignore
import httpx
from prompt_toolkit import Application, print_formatted_text
from prompt_toolkit.application import get_app
from prompt_toolkit.buffer import Buffer
from prompt_toolkit.formatted_text import HTML, ANSI, to_formatted_text
from prompt_toolkit.history import FileHistory
from prompt_toolkit.layout import FloatContainer, Float, WindowAlign, Dimension
from prompt_toolkit.layout.containers import HSplit, VSplit, Window
from prompt_toolkit.layout.controls import BufferControl, FormattedTextControl
from prompt_toolkit.layout.layout import Layout
from prompt_toolkit.layout.menus import CompletionsMenu
from prompt_toolkit.key_binding import KeyBindings
from prompt_toolkit.styles import Style
from prompt_toolkit.widgets import Dialog, Label, Button, TextArea, RadioList, CheckboxList, Frame

from .complete import PromCompleter
from .plot import plot
from .query import query_range


TIMEFMT = "%Y-%m-%d %H:%M:%S"


@dataclass
class TimeWindow:

    size: float = 3600
    end: Optional[float] = None

    def zoom(self, factor: int):
        if self.end is None:
            self.end = time()
        # This probably only works for factors -1 and 1
        if factor < 0:
            pan = 1 / (factor * 4)
        else:
            pan = 1 / (factor * 2)
        self.end += self.size * pan
        self.size *= 2**factor

    def pan(self, amount: float):
        if self.end is None:
            self.end = time()
        self.end += amount * self.size

    @property
    def limits(self) -> Tuple[float, float]:
        end = self.end or time()
        return end - self.size, end

    @property
    def limits_datetime(self) -> Tuple[datetime, datetime]:
        start, end = self.limits
        return (datetime.fromtimestamp(start).astimezone(tz=None),
                datetime.fromtimestamp(end).astimezone(tz=None))

    def __str__(self):
        start, end = self.limits_datetime
        return f"[{start.strftime(TIMEFMT)} - {end.strftime(TIMEFMT)}]"


async def promenade(prometheus_url):

    # search = Buffer()  # Editable buffer.
    # attribute_completer = AttributeCompleter(hdbpp, controlsystems[1])
    username = os.environ.get("PROM_USER")
    password = os.environ.get("PROM_PASS")
    auth = (username, password) if username and password else None
    async with httpx.AsyncClient(base_url=f"{prometheus_url}/api/v1",
                                 auth=auth) as client:
        completer = PromCompleter(client)

        async def get_data(client: httpx.AsyncClient, q: str, time_window: TimeWindow
                           ) -> Tuple[str, int]:
            width, height = shutil.get_terminal_size((80, 20))
            height -= len(q.splitlines()) + 4
            # height = min(height // 2, width // 2)  # Find a reasonable height
            start, end = time_window.limits
            lines, trunc = await query_range(client, q, start, end,
                                             time_window.size / (width * 2))
            return plot(lines, width, height, x_limits=time_window.limits_datetime), trunc

        def show(output: str, trunc: bool):
            plot_control.text = ANSI(output)
            get_app().invalidate()
            # if trunc:
            #     print(f"(Ran out of colors, skipping {trunc} results)")

        query = None

        def on_enter(event):
            nonlocal query
            query = event.document.text
            asyncio.create_task(update_plot(query))

        time_window = TimeWindow(end=time())

        async def update_plot(query):
            try:
                output, trunc = await get_data(client, query, time_window)
                show(output, trunc)
            except RuntimeError as e:
                show(f"\033[1;31m{e}", 0)

        data_dir = user_data_dir("promenade")  # For persisting history etc
        os.makedirs(data_dir, exist_ok=True)
        history = FileHistory(data_dir + "/history")

        search = TextArea(multiline=True,
                          completer=completer,
                          history=history,
                          accept_handler=on_enter)

        toolbar_control = FormattedTextControl(text=time_window.__str__)

        plot_control = FormattedTextControl(text="")

        root_container = FloatContainer(
            content=HSplit([
                search,
                Window(height=1, char=" ", content=toolbar_control,
                       align=WindowAlign.RIGHT, style="bg:ansigray fg:ansiblack"),
                Window(content=plot_control, height=Dimension(weight=3)),
            ]),
            floats=[
                Float(xcursor=True, ycursor=True,
                      content=CompletionsMenu(max_height=10, scroll_offset=1))
            ]
        )

        layout = Layout(root_container)

        kb = KeyBindings()

        @kb.add('s-left')
        async def _(_):
            time_window.pan(-0.5)
            asyncio.create_task(update_plot(query))

        @kb.add('s-right')
        async def _(_):
            time_window.pan(0.5)
            asyncio.create_task(update_plot(query))

        @kb.add('s-up')
        async def _(_):
            time_window.zoom(-1)
            asyncio.create_task(update_plot(query))

        @kb.add('s-down')
        async def _(_):
            time_window.zoom(1)
            asyncio.create_task(update_plot(query))

        @kb.add('c-q')
        def exit_(event):
            """
            Pressing Ctrl-Q will exit the user interface.
            """
            event.app.exit()

        app = Application(
            layout=layout,
            key_bindings=kb,
            mouse_support=True,
            full_screen=True
        )
        await app.run_async()


def main():
    from argparse import ArgumentParser
    import sys

    parser = ArgumentParser()
    parser.add_argument("prometheus", nargs="?", default="http://localhost:9090")
    parser.add_argument("--version", action="store_true")

    args = parser.parse_args()

    if args.version:
        from ._version import version
        print(version)
        sys.exit()

    asyncio.run(promenade(args.prometheus))


if __name__ == "__main__":
    main()
