from datetime import datetime
import json
from typing import Tuple, Dict, List

import httpx


def deduplicate(metrics):
    """
    Take a list of metrics (dicts) and return a list of dicts
    only containing the unique key-value pairs. I.e. remove any
    common key-value pairs. The idea is to make the legend take
    less space on screen.
    """
    if len(metrics) < 2:
        yield from metrics
        return
    common = set.intersection(*map(set, (m.items() for m in metrics)))
    for m in metrics:
        ms = set(m.items())
        yield dict(sorted(ms - common))


def make_series_string(metric: Dict) -> str:
    name = metric.pop('__name__', '')
    labels = ",".join(f'{label}="{value}"' for label, value in metric.items())
    return f"{name}{{{labels}}}"


async def query_range(client: httpx.AsyncClient,
                      query: str, start: float, end: float, step: float,
                      maxlines: int = 14) -> Tuple[List, int]:

    """ Make a query for Prometheus metrics over a time range """

    params = {
        "query": query,
        "start": start,
        "end": end,
        "step": step,
    }
    try:
        response = await client.get("/query_range", params=params)
    except httpx.HTTPError as e:
        raise RuntimeError(f"Could not connect: {e}")
    if response.status_code != httpx.codes.OK:
        try:
            # Presumably we get some error information back
            result = response.json()
        except json.decoder.JSONDecodeError:
            raise RuntimeError(f"Failed to decode response!? {response.text}")
        raise RuntimeError(f"Error {response.status_code}: {result['error']}")

    try:
        data = response.json()["data"]["result"]
    except json.decoder.JSONDecodeError:
        raise RuntimeError(response.text)
    lines = []
    short_data = data[:maxlines]
    metrics = [result["metric"] for result in short_data]
    compact_metrics = deduplicate(metrics)
    for i, (result, metric) in enumerate(zip(short_data, compact_metrics)):
        values = result["values"]
        if not values:
            continue
        xsn, yst = zip(*result["values"])
        xs = [datetime.fromtimestamp(x).astimezone(tz=None) for x in xsn]
        ys = [float(y) for y in yst]
        title = make_series_string(metric)
        lines.append((xs, ys, title))
    return lines, -min(0, maxlines - len(data))
