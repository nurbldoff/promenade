import asyncio
from argparse import ArgumentParser

from .prompt import main


def run():
    parser = ArgumentParser()
    parser.add_argument("prometheus", nargs="?", default="http://localhost:9090")
    args = parser.parse_args()
    asyncio.run(main(args.prometheus))
