"""Allow plotting x, y points as lines, with axes"""

from datetime import datetime, timedelta
from typing import Sequence, Tuple, Optional

from .canvas import BrailleCanvas, ANSI_COLORS, ANSI_RESET


TimeSeries = Tuple[Sequence[datetime], Sequence[float], str]


def get_reasonable_limits(a: float, b: float
                          ) -> Tuple[Tuple[float, str], Tuple[float, str]]:
    return (round(a, 3), f"{a:.2e}"), (round(b, 3), f"{b:.2e}")


def pack_labels(labels: Sequence[Tuple[str, int]], width: int):
    # TODO this is pretty broken
    output = [""]
    for label, color in labels:
        lines = len(label) // (width) + 1
        if lines > 1:
            parts = [label[i*width:i*width+width] for i in range(lines)]
            for part in parts:
                output.append(f"{ANSI_COLORS[color]}{part}")
            output.append("")
        else:
            if len(output[-1]) + len(label) + 2 >= width:
                output.append("")
            output[-1] += f"{ANSI_COLORS[color]}{label} "
    return [o.strip() for o in output if o]


def get_time_ticks(start: datetime,
                   end: datetime
                   ) -> Tuple[Sequence[Tuple[datetime, str]], str]:

    """
    Return a list of reasonable axis ticks for the given range.
    A "tick" is a tuple of a datetime position and a string label.
    Also returns a rough length of the range in human language, e.g. "~4 days".
    """

    ticks = []
    delta = end - start
    dt = delta.total_seconds()
    desc = ""

    if delta.days > 60:
        t0 = datetime(start.year, start.month, start.day).astimezone(tz=None)
        increment = timedelta(days=1)
        t = t0
        while t < end:
            if t.day == 1:
                ticks.append((t, t.strftime("%b")))
            t += increment
        desc = f"~{delta.days // 30} months"

    elif delta.days > 14:
        # find first monday
        t0 = datetime(start.year, start.month, start.day).astimezone(tz=None)
        while t0.weekday() != 0:
            t0 += timedelta(days=1)
        increment = timedelta(days=7)
        t = t0
        while t < end:
            ticks.append((t, t.strftime("%b %d")))
            t += increment
        desc = f"~{delta.days // 7} weeks"

    elif delta.days > 2:
        t0 = datetime(start.year, start.month, start.day).astimezone(tz=None)
        increment = timedelta(days=1)
        t = t0 + increment
        while t < end:
            ticks.append((t, str(t.day)))
            t += increment
        desc = f"~{delta.days} days"

    elif dt > 3600 * 24:
        t0 = datetime(start.year, start.month, start.day, start.hour).astimezone(tz=None)
        increment = timedelta(hours=1)
        t = t0 + increment
        while t < end:
            if t.hour % 6 == 0:
                ticks.append((t, str(f"{t.hour:02d}:00")))
            t += increment
        desc = f"~{round(dt / 3600)} hours"

    elif dt > 3600 * 6:
        t0 = datetime(start.year, start.month, start.day, start.hour).astimezone(tz=None)
        increment = timedelta(hours=1)
        t = t0 + increment
        while t < end:
            if t.hour % 3 == 0:
                ticks.append((t, str(f"{t.hour:02d}:00")))
            t += increment
        desc = f"~{round(dt / 3600)} hours"

    elif dt > 3600:
        t0 = datetime(start.year, start.month, start.day, start.hour).astimezone(tz=None)
        increment = timedelta(hours=1)
        t = t0 + increment
        while t < end:
            ticks.append((t, str(f"{t.hour:02d}:00")))
            t += increment
        desc = f"~{round(dt / 3600)} hours"

    elif dt > 900:
        t0 = datetime(start.year, start.month, start.day, start.hour,
                      (start.minute // 15) * 15).astimezone(tz=None)
        increment = timedelta(minutes=15)
        t = t0 + increment
        while t < end:
            ticks.append((t, str(f"{t.hour:02d}:{t.minute:02d}")))
            t += increment
        desc = f"~{round(dt / 60)} minutes"

    elif dt > 300:
        t0 = datetime(start.year, start.month, start.day, start.hour,
                      (start.minute // 5) * 5).astimezone(tz=None)
        increment = timedelta(minutes=1)
        t = t0 + increment
        while t < end:
            if t.minute % 5 == 0:
                ticks.append((t, str(f"{t.hour:02}:{t.minute:02}")))
            t += increment
        desc = f"~{round(dt / 60)} minutes"

    elif dt > 60:
        t0 = datetime(start.year, start.month, start.day, start.hour, start.minute).astimezone(tz=None)
        increment = timedelta(seconds=60)
        t = t0 + increment
        while t < end:
            ticks.append((t, str(t.minute)))
            t += increment
        desc = f"~{round(dt / 60)} minutes"

    else:
        t0 = datetime(start.year, start.month, start.day, start.hour, start.minute,
                      (start.second // 10) * 10).astimezone(tz=None)
        increment = timedelta(seconds=10)
        t = t0 + increment
        while t < end:
            ticks.append((t, str(t.second)))
            t += increment
        desc = f"~{round(dt)} seconds"

    return ticks, desc


def get_screen_position(scale_width: float,
                        screen_width: int,
                        value: float) -> int:
    return round((value / scale_width) * screen_width)


TIMESTAMP = "%Y-%m-%d %H:%M:%S%z"


def plot(
        data: Sequence[TimeSeries], width=80, height=25,
        x_limits: Tuple[Optional[datetime], Optional[datetime]] = (None, None),
        y_limits: Tuple[Optional[float], Optional[float]] = (None, None),
        lines: bool = True, show_x_axis=True, show_y_axis=False, show_legend=True,
) -> str:

    """Draw a number of timeseries as lines, along with X and Y axes"""

    xmin, xmax = x_limits
    ymin, ymax = y_limits

    xmin_set = xmin is not None
    xmax_set = xmax is not None
    ymin_set = ymin is not None
    ymax_set = ymax is not None

    for xs, ys, *_ in data:
        if not xmin_set:
            if xmin is None:
                xmin = min(xs)
            else:
                xmin = min(xmin, min(xs))
        if not xmax_set:
            if xmax is None:
                xmax = max(xs)
            else:
                xmax = max(xmax, max(xs))
        if not ymin_set:
            if ymin is None:
                ymin = min(ys)
            else:
                ymin = min(ymin, min(ys))
        if not ymax_set:
            if ymax is None:
                ymax = max(ys)
            else:
                ymax = max(ymax, max(ys))

    if xmin is None or xmax is None:
        xmax = datetime.now()
        xmin = xmax - timedelta(hours=1)
    if ymin is None or ymax is None:
        ymin = -1
        ymax = 1

    if ymin == ymax:
        # Flat line; no obvious way to get limits
        # TODO handle this some better way?
        ymin -= 1
        ymax += 1

    dx = xmax - xmin
    dy = ymax - ymin

    labels = ((label, color) for color, (_, _, label) in enumerate(data))
    legend = pack_labels(labels, width-1)
    legend_height = len(legend)
    canvas = BrailleCanvas(width, max(10, height - (3 if show_x_axis else 0) - legend_height))

    canvas_w = canvas.w - show_y_axis  # Avoid losing pixels at the edges
    canvas_h = canvas.h - 1

    # Draw the plots
    for color, (xs, ys, _, *maybe_color) in enumerate(data):
        prev_p = None
        if len(maybe_color) == 1:
            color, = maybe_color
        for x, y in zip(xs, ys):
            cx = get_screen_position(dx.total_seconds(),
                                     canvas_w,
                                     (x - xmin).total_seconds())
            cy = get_screen_position(dy, canvas_h, ymax - float(y))
            p = (cx, cy)
            if lines and prev_p:
                canvas.draw_line(prev_p, p, color)
            else:
                canvas.draw_pixel(*p, color)
            prev_p = p

    xmin_repr = xmin.astimezone(tz=None).strftime(TIMESTAMP)
    xmax_repr = xmax.astimezone(tz=None).strftime(TIMESTAMP)
    (ymin, ymin_repr), (ymax, ymax_repr) = get_reasonable_limits(ymin, ymax)

    # Overlay the y max/min on the plot
    canvas.draw_text(0, 0, f"▲{ymax_repr}", None)
    canvas.draw_text(0, canvas.height - 1, f"▼{ymin_repr}", None)

    graph = canvas.render()

    # Time axis
    if show_x_axis:
        x_ticks, x_desc = get_time_ticks(xmin, xmax)
        x_axis_ticks = ""
        x_axis_inner = ""
        if x_ticks:
            x = xmin
            d0 = xmin
            # Draw ticks
            for tick, desc in x_ticks:
                d = tick - d0
                pos = get_screen_position(dx.total_seconds(),
                                          canvas.width,
                                          d.total_seconds())
                x_axis_ticks += ' ' * (pos - len(x_axis_ticks) - (len(desc)) // 2) + desc
                x_axis_inner += '─' * (pos - len(x_axis_inner)) + "┬"
            x_axis_inner += '─' * (canvas.width - len(x_axis_inner))
        else:
            x_axis_inner = '─' * (canvas.width)
        x_axis = (f"{x_axis_inner}\n" +
                  x_axis_ticks + "\n" +
                  f"◀{xmin_repr}{x_desc.center(canvas.width - len(xmin_repr) - len(xmax_repr) - 2)}{xmax_repr}▶" + "\n")
    else:
        x_axis = ""
    # Put it all together
    y_axis = "|" if show_y_axis else ""
    return "\n".join([
        *(y_axis + line for line in graph),
        x_axis,
        *legend,
    ]) + ANSI_RESET
