from datetime import datetime
from dataclasses import dataclass
import os
import shutil
from time import time
from typing import Tuple, Optional

from appdirs import user_data_dir  # type: ignore
import httpx
from prompt_toolkit import PromptSession, print_formatted_text
from prompt_toolkit.application import run_in_terminal
from prompt_toolkit.filters import Condition
from prompt_toolkit.formatted_text import HTML, ANSI
from prompt_toolkit.history import FileHistory
from prompt_toolkit.key_binding import (KeyBindings, ConditionalKeyBindings,
                                        merge_key_bindings)
from prompt_toolkit.lexers import PygmentsLexer
from prompt_toolkit.patch_stdout import patch_stdout
from prompt_toolkit.shortcuts import CompleteStyle
from prompt_toolkit.styles import Style

try:
    from pygments.lexers.promql import PromQLLexer  # type: ignore
    from prompt_toolkit.styles.pygments import style_from_pygments_cls
    from pygments.styles import get_style_by_name  # type: ignore
except ImportError:
    PromQLLexer = None

from .complete import PromCompleter
from .plot import plot
from .query import query_range


TIMEFMT = "%Y-%m-%d %H:%M:%S"


@dataclass
class TimeWindow:

    size: float = 3600
    end: Optional[float] = None

    def zoom(self, factor: int):
        if self.end is None:
            self.end = time()
        # This probably only works for factors -1 and 1
        if factor < 0:
            pan = 1 / (factor * 4)
        else:
            pan = 1 / (factor * 2)
        self.end += self.size * pan
        self.size *= 2**factor

    def pan(self, amount: float):
        if self.end is None:
            self.end = time()
        self.end += amount * self.size

    @property
    def limits(self) -> Tuple[float, float]:
        end = self.end or time()
        return end - self.size, end

    @property
    def limits_datetime(self) -> Tuple[datetime, datetime]:
        start, end = self.limits
        return (datetime.fromtimestamp(start).astimezone(tz=None),
                datetime.fromtimestamp(end).astimezone(tz=None))


async def main(prometheus_url):

    query = None
    time_window = TimeWindow()

    bindings = KeyBindings()

    @bindings.add('c-q')
    def _(event):
        event.app.exit()

    view_bindings = KeyBindings()

    @view_bindings.add('s-left')
    async def _(_):
        time_window.pan(-0.5)
        output, trunc = await get_data(client, query, time_window)
        run_in_terminal(lambda: show(output, trunc))

    @view_bindings.add('s-right')
    async def _(_):
        time_window.pan(0.5)
        output, trunc = await get_data(client, query, time_window)
        run_in_terminal(lambda: show(output, trunc))

    @view_bindings.add('s-up')
    async def _(_):
        time_window.zoom(-1)
        output, trunc = await get_data(client, query, time_window)
        run_in_terminal(lambda: show(output, trunc))

    @view_bindings.add('s-down')
    async def _(_):
        time_window.zoom(1)
        output, trunc = await get_data(client, query, time_window)
        run_in_terminal(lambda: show(output, trunc))

    @Condition
    def has_query() -> bool:
        return query is not None

    cond_view_bindings = ConditionalKeyBindings(
        key_bindings=view_bindings,
        filter=has_query)

    def toolbar():
        if time_window.end is None:
            timespan = f"Last {time_window.size} s"
        else:
            a, b = time_window.limits_datetime
            timespan = f"{a.strftime(TIMEFMT)} - {b.strftime(TIMEFMT)}"
        return HTML(f'[{timespan}]')

    data_dir = user_data_dir("promenade")  # For persisting history etc
    os.makedirs(data_dir, exist_ok=True)
    history = FileHistory(data_dir + "/history")

    if PromQLLexer:
        lexer = PygmentsLexer(PromQLLexer)
        # TODO Colors look weird... can we get ANSI only?
        style = style_from_pygments_cls(get_style_by_name('emacs'))
    else:
        lexer = None
        style = Style.from_dict({
            # 'prompt': 'bg:#ff0066 #ffffff',
            # 'rprompt': 'bg:#770044 #ffffff',
        })

    username = os.environ.get("PROM_USER")
    password = os.environ.get("PROM_PASS")
    auth = (username, password) if username and password else None
    async with httpx.AsyncClient(base_url=f"{prometheus_url}/api/v1",
                                 auth=auth) as client:

        completer = PromCompleter(client)

        session = PromptSession(
            lexer=lexer,
            completer=completer,
            complete_style=CompleteStyle.MULTI_COLUMN,
            history=history,
            key_bindings=merge_key_bindings([bindings,
                                             cond_view_bindings]),
            # complete_while_typing=False,
            rprompt=toolbar, style=style)

        # Main loop
        while True:
            with patch_stdout():
                try:
                    query = await session.prompt_async('> ')
                except KeyboardInterrupt:
                    continue
                except EOFError:
                    break
            if query is None:
                # Looks like Ctrl-Q
                break
            output, trunc = await get_data(client, query, time_window)
            show(output, trunc)


async def get_data(client: httpx.AsyncClient, q: str, time_window: TimeWindow
                   ) -> Tuple[str, int]:
    width, height = shutil.get_terminal_size((80, 20))
    height = min(height // 2, width // 2)  # Find a reasonable height
    start, end = time_window.limits
    try:
        lines, trunc = await query_range(client, q, start, end,
                                         time_window.size / (width * 2))
    except RuntimeError as e:
        return f"\033[1;31m{e}", 0
    return plot(lines, width, height, x_limits=time_window.limits_datetime), trunc


def show(output: str, trunc: bool):
    print_formatted_text(ANSI(output))
    if trunc:
        print(f"(Ran out of colors, skipping {trunc} results)")
