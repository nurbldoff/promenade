# Promenade

A friendly CLI to [Prometheus](http://prometheus.io). Currently under heavy development.

![Screenshot](screenshot.png)

## Features
- Completion of basic PromQL syntax, with "live" values (work in progress, may work)
- Plotting using Braille characters and colors (inspired by [Plotille](https://github.com/tammoippen/plotille) and similar projects)
- Keyboard shortcuts to quickly browse data

## Key bindings
- `Alt-enter` submit query
- `Crtl-Q` Exit
- `Ctrl-C` Cancel
- `Shift-left/right` pan plot time window
- `Shift-up/down` Zoom in/out 

## Missing features
- Table output
- Explicitly selecting a time window
- Compact legend

## Usage

### Install

    python -m venv env
    env/bin/pip install .

### Run

    env/bin/promenade <prometheus-url>
        
The URL should be everything up until, but not including, `/api/v1`. If your prometheus API is behind HTTP authorization, you can provide username and password via the environment variables `PROM_USER` and `PROM_PASS`.

### Test:

    env/bin/pip install .[tests]
    env/bin/pytest
