from datetime import datetime, timedelta
from random import randint

from promenade.plot import get_time_ticks


def test_get_time_ticks_basic():
    # Take a random date
    start = datetime(randint(2000, 2020), randint(1, 12), randint(1, 27)).astimezone(tz=None)
    end = (start + timedelta(days=1)).astimezone(tz=None)
    ticks, desc = get_time_ticks(start, end)

    expected_ticks = ["03:00", "06:00", "09:00", "12:00", "15:00", "18:00", "21:00"]
    assert [t[1] for t in ticks] == expected_ticks


def test_get_time_ticks_weeks():
    start = datetime(randint(2000, 2020), randint(1, 12), randint(1, 27)).astimezone(tz=None)
    end = (start + timedelta(days=21)).astimezone(tz=None)
    ticks, desc = get_time_ticks(start, end)

    assert len(ticks) == 3
